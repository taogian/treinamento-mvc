// Arguments passed into this controller can be accessed via the `$.args` object directly or:

var args = $.args;
var Map = require('ti.map');

var meView = Map.createAnnotation({
    latitude: args.coord[0],
    longitude: args.coord[1],
    title: args.end,
    pincolor: Map.ANNOTATION_PURPLE,
    myid:1 // Custom property to uniquely identify this annotation.
});

var mapview = Map.createView({
    mapType: Map.NORMAL_TYPE,
    region: {latitude: -22.9053516, longitude: -43.1956282,
            latitudeDelta:0.01, longitudeDelta:0.01},
    animate:true,
    regionFit:true,
    userLocation:false,
    annotations:[meView]
});
 
 var view = Ti.UI.createView();
 
 view.add(mapview);
 
$.win.add(view);

$.win.open();