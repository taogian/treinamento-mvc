tableData = [];

var data = [

{end: 'Av. Mal. Floriano, 37', coord: [-22.9009162,-43.1795591]},
{end: 'R. Marquês de Sapucaí, 36', coord: [-22.9110362,-43.2080549]},
{end: 'Av. Pres. Castelo Branco, Portão 3', coord: [-22.9170841,-43.2371515]},
{end: 'Av. Min. Edgard Romero, 239' , coord: [-22.878649,-43.3326535]},
{end: 'Estr. do Camboatá, 2300', coord: [-22.8405279,-43.4051375]},
{end: 'Av. Getúlio Vargas, 1831', coord: [-22.8068269,-43.4285264]},

];

var table = Ti.UI.createTableView({ objName: 'table' });

for (var i = 0; i <= 5; i++){
  var row = Ti.UI.createTableViewRow({
    className: 'row',
    objName: 'row',
    touchEnabled: true,
    height: 100,
    backgroundColor: 'black',
    
  });

var addressView = Ti.UI.createView({
	width: Ti.UI.FILL, height: '100%',
	top: '0%' 
});

var label01 = Ti.UI.createLabel({
	text: data[i].end,
	font: {fontSize: 22},
	maxLines: 0
});

var coordView = Ti.UI.createView({
	width: Ti.UI.SIZE, height: '100%',
	top: '30%' 
});

var label02 = Ti.UI.createLabel({
	text: data[i].coord.toString(),
	font: {fontSize: 12} 
});
  
  addressView.add(label01);
  coordView.add(label02);
  row.add(addressView);
  row.add(coordView);
  tableData.push(row);
}

table.setData(tableData);

table.addEventListener('click', function(e){
  var arg = data[e.index];
  //alert(arg);
  Alloy.createController('map', arg).getView().open();
});

$.win.add(table);
$.win.open();
